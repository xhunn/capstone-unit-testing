const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
    app.get('/currency', (req, res) => {
        return res.send({
            exchangeRates: exchangeRates
        })
    })

    /* 
        CAPSTONE:
        1. Check if post /currency is running
        2. Check if post /currency returns status 400 if name is missing
        3. Check if post /currency returns status 400 if name is not a string
        4. Check if post /currency returns status 400 if name is empty string
        5. Check if post /currency returns status 400 if ex is missing
        6. Check if post /currency returns status 400 if ex is not an object
        7. Check if post /currency returns status 400 if ex is empty object
        8. Check if post /currency returns status 400 if alias is missing
        9. Check if post /currency returns status 400 if alias is not an string
        10. Check if post /currency returns status 400 if alias is empty string
        11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
        12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
    */

    app.post('/currency', (req, res) => {
        console.log(Array.isArray(req.body))
        // CHECKER
        if (Array.isArray(req.body)) {
            for (let i = 0; i < req.body.length; i++) {
                let toCheck = req.body[i].alias;
                for (let j = 0; j < req.body.length; j++) {
                    let toCompare = req.body[j].alias
                    if (i == j) {
                        //pass
                    } else {
                        console.log(toCheck + " vs " + toCompare)
                        if (toCheck == toCompare) {
                            console.log("Returned 400")
                            return res.status(400).send({
                                "Error": "Duplicate alias found"
                            })  
                        } else {
                            console.log("Returned 200")
                            return res.send(200).send({
                                "Success": "All fields complete and no duplicates"
                            })
                        }
                    }
                }
            }  
        } 

        // NAME
        if (!req.body.hasOwnProperty('name')) {
            return res.status(400).send({
                "Error": "NAME Property isn't indicated"
            })
        }

        if (typeof req.body.name !== 'string') {
            return res.status(400).send({
                "Error": "NAME Property isn't a string"
            })
        }

        if (req.body.name == '') {
            return res.status(400).send({
                "Error": "NAME Property should not be empty"
            })
        }

        // EXCHANGE
        if (!req.body.hasOwnProperty('ex')) {
            return res.status(400).send({
                "Error": "EXCHANGE Property isn't indicated"
            })
        }

        if (typeof req.body.ex != 'object') {
            return res.status(400).send({
                "Error": "EXCHANGE Property isn't an object"
            })
        }

        if (Object.entries(req.body.ex).length === 0) {
            return res.status(400).send({
                "Error": "EXCHANGE Property should not be an empty object"
            })
        }

        // ALIAS
        if (!req.body.hasOwnProperty('alias')) {
            return res.status(400).send({
                "Error": "ALIAS Property isn't indicated"
            })
        }

        if (typeof req.body.alias != 'string') {
            return res.status(400).send({
                "Error": "ALIAS Property isn't a string"
            })
        }

        if (req.body.alias == '') {
            return res.status(400).send({
                "Error": "ALIAS Property should not be an empty string"
            })
        }
    })
}