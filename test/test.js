const { assert } = require('chai');

const chai = require('chai');
const http = require('chai-http')
chai.use(http)

/* 
    CAPSTONE:
    1. Check if post /currency is running
    2. Check if post /currency returns status 400 if name is missing
    3. Check if post /currency returns status 400 if name is not a string
    4. Check if post /currency returns status 400 if name is empty string
    5. Check if post /currency returns status 400 if ex is missing
    6. Check if post /currency returns status 400 if ex is not an object
    7. Check if post /currency returns status 400 if ex is empty object
    8. Check if post /currency returns status 400 if alias is missing
    9. Check if post /currency returns status 400 if alias is not an string
    10. Check if post /currency returns status 400 if alias is empty string
    11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
    12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates
*/

describe('TDD Exchange Rates', () => {
    it('Check if post /currency is running', () => {
        chai.request('http://localhost:5000').get('/currency').end((err, res) => {
            assert.notEqual(res, undefined);
        })
    })
    
    it('Check if post /currency returns status 400 if name is missing', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: "dollars",
                ex: {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if name is not a string', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: "dollars",
                name: 123,
                ex: {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if name is empty string', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: "dollars",
                name: '',
                ex: {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if ex is missing', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: "dollars",
                name: "United States Dollar"
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if ex is not an object', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: "dollars",
                name: "United States Dollar",
                ex: 'exchange'
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if ex is empty object', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: "dollars",
                name: "United States Dollar",
                ex: {}
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })
    
    it('Check if post /currency returns status 400 if alias is missing', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                name: "United States Dollar",
                ex: {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if alias is not an string', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: 123,
                name: "United States Dollar",
                ex: {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if alias is empty string', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send({
                alias: '',
                name: "United States Dollar",
                ex: {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send([{
                alias: 'Pounds',
                name: "Euros",
                ex: {
                    'peso': 56.71,
                    'won': 1331.19,
                    'yen': 128.12,
                    'yuan': 7.17,
                    'usd': 1.13
                }
            }, {
                alias: 'Pounds',
                name: "Ruble",
                ex: {
                    'peso': .69,
                    'won': 16.10,
                    'yen': 1.55,
                    'yuan': 0.087,
                    'usd': 0.014
                }
            }])
            .end((err, res) => {
                assert.equal(res.status, 400)
                done()
            })
    })

    it('Check if post /currency returns status 200 if all fields are complete and there are no duplicates', (done) => {
        chai.request('http://localhost:5000')
            .post('/currency')
            .type('json')
            .send([{
                alias: 'Pounds',
                name: "Euros",
                ex: {
                    'peso': 56.71,
                    'won': 1331.19,
                    'yen': 128.12,
                    'yuan': 7.17,
                    'usd': 1.13
                }
            }, {
                alias: 'Russian Rubble',
                name: "Ruble",
                ex: {
                    'peso': .69,
                    'won': 16.10,
                    'yen': 1.55,
                    'yuan': 0.087,
                    'usd': 0.014
                }
            }])
            .end((err, res) => {
                assert.equal(res.status, 200)
                done()
            })
    })
})
